# Go Game
> Written by: Noah Barnes, Allen Clark, David Tovar

## USER README
> This readme will cover how to install and run our software from an executable as-well-as how to run the program through the Lua/LOVE environment.

### Playing through the Lua/LOVE environment
> LOVE is a framework aimed at game development in the Lua programming language. It is open source, and can be run on almost every system out there. From Windows to iOS and Android, as long as Lua can be run on it, LOVE can be to.

#### Download Lua
1. Lua can be found [here](https://www.lua.org/ftp/).
  - *This program was written in Lua 5.3.4 and it is recommended to use that release.*
2. Follow the install instructions that are relevant to your system.

> ***NOTE***
>
>We built Lua on Unix-like systems the steps that follow were used on Ubuntu 16.04. For other systems see Lua's installation documentation that can be found [here](https://www.lua.org/manual/5.3/readme.html).

#### Install Lua Linux Ubuntu 16.04+
1. Use a terminal to navigate to the directory's top level. The directory should be named lua-5.3-4.
2. Run the command `make linux` from this directory.
  - *If the installation process fails due to a dependency simply install that missing package through your system's package manager. "I had to install `libreadline-dev` before I could get the install step to complete.": Allen*
3. To install Lua use the command `make install`.
4. To see if Lua was successfully installed run the command `lua -v`.
  - *You should see the following `Lua 5.3.4  Copyright (C) 1994-2017 Lua.org, PUC-Rio`*
  - *If you wish you can run the command `lua` in your terminal to transform your prompt into a Lua prompt. To return to bash press `ctrl + c`.*
5. Lua installation complete.
> ***NOTE***
>
> All installation instructions for Lua can be found on Lua's website which can be reached [here](https://www.lua.org/manual/5.3/readme.html).

#### Download LOVE
1. Love can be found [here](https://love2d.org/).
2. Download the version that corresponds with your system.
  -*This program was written on the 0.10.2 release and it is recommended to use that release*

> ***NOTE***
>
> There are multiple ways to install LOVE. The method that was used to install LOVE on Ubuntu 16.04 was by adding the **PPA** and installing it through **apt-get**. The flowing outline the steps to install LOVE using this method.

#### Install LOVE
1. Details relating to LOVE's PPA can be found [here](https://launchpad.net/~bartbes/+archive/ubuntu/love-stable).
2. Open a terminal and enter the command `sudo add-apt-repository ppa:bartbes/love-stable` to add the LOVE repository to your system.
3. Run the command `sudo apt-get update` to update your repositories.
4. Install LOVE and all dependencies with the command `sudo apt-get install love`.
5. To test if love was successfully installed run the command `love --version`. The following should be printed to your console:
  > LOVE 0.10.2 (Super Toast)

6. One last test; run the command `love` and a window should appear. If both the previous step and this step competed successfully then LOVE is completely installed on your system.
> ***NOTE***
>
> All instructions on how to install LOVE can be found on LOVE's website located [here](https://love2d.org/).

> ***NOTE***
>
> The program can be run by using the **.exe** file on Windows based systems. For all other systems, the program should be run using the LOVE interface. The following steps outline how to run the program through LOVE's interface.

#### Running the game
1. In your terminal navigate to the directory containing the file named **main.lua**.
  - *If you are using the files that we provided then this directory will be **cs.354_lua***.
2. Run the following command `love ../[containing directory]`,
  - *If you are using the files that we provided then the "containing directory" should be **project0***.
  - Example command `love ../project0`.

### Running the Game on Windows
1. In Windows Explorer navigate to the *go_game* folder located in the **cs.354_lua** folder.
2. Double click on **Go.exe** to start the program.

### Game Controls
* "left-click" - place stone
* "r" - start a new game
* "e" - pass turn
