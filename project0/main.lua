grid = {}
gridSize = 13
cellSize = 40
love.graphics.setBackgroundColor( 155, 155, 155 )
whiteTurn = true

-- mouse values
toggleClick = true
wasReleased = true
--

text = "White's Turn"
sound_place = love.audio.newSource("placesound.wav")
sound_capture = love.audio.newSource("capturesound.wav")

-- scores --
local scores = {
	blackScore;
	whiteScore;
}
--

local groupSize -- TODO this should not be global its just bad programming
								-- Score is totaled in clearStone and added in checkGroup

-- PassTurns
local blackPassTurn
local whitePassTurn
--

-- Total stones
local totWhite
local totBlack

local apply

--Initialize grid set to 0 (default color) for each tile
--[
	Called once at the beginning, unless otherwise invoked.
--]
function love.load()
	white = love.graphics.newImage("white.png")
	black = love.graphics.newImage("black.png")
	board = love.graphics.newImage("wood_texture.jpg")

	-- defaults
	scores.whiteScore = 0
	scores.blackScore = 0

	groupSize = 0

	whitePassTurn = false
	blackPassTurn = false
	whiteTurn = true

	totWhite = 0
	totBlack = 0

	apply = true
	--

	for x = 1, gridSize do
	  grid[x] = {}
	  for y = 1, gridSize do
	    grid[x][y] = " "
	  end
	end
end -- load()

function love.update()
	if not whitePassTurn or not blackPassTurn then
		--Keep track of if the mouse is down and has been released
		local mouseDown = love.mouse.isDown(1)

		if mouseDown == false then
			wasReleased = true
		end
		if mouseDown and wasReleased then
			toggleClick = true
		else
			toggleClick = false
		end

		--Current mouse tile location
	    selectedX = math.floor(love.mouse.getX() / cellSize)
	    selectedY = math.floor(love.mouse.getY() / cellSize)

		--Limit selected x y to be within the grid size
		if selectedX < 1 then
			selectedX = 1
		end
		if selectedY < 1 then
			selectedY = 1
		end
		if selectedX > gridSize then
			selectedX = gridSize
		end
		if selectedY > gridSize then
			selectedY = gridSize
		end

		--Change tile value depending on which color's turn it is
	    if toggleClick
	    and selectedX <= gridSize
	    and selectedY <= gridSize
	    and grid[selectedX][selectedY] == " " then
	    	if whiteTurn and placeCheck(selectedX, selectedY, "white") then
	      	grid[selectedX][selectedY] = "white"
					checkStone(selectedX,selectedY,"black")
					sound_place:play()

					-- increase total white stones
					totWhite = totWhite + 1

					--toggle which colored turn it is
					whiteTurn = not whiteTurn
	      elseif whiteTurn == false and placeCheck(selectedX, selectedY, "black") then
	      	grid[selectedX][selectedY] = "black"
					checkStone(selectedX,selectedY,"white")
					sound_place:play()

					-- increase total black stones
					totBlack = totBlack + 1

					--toggle which colored turn it is
					whiteTurn = not whiteTurn
	      end

				blackPassTurn = false -- resets passes
				whitePassTurn = false -- resets passes
				wasReleased = false
	    end
		else
			print(blackPassTurn)
			print(whitePassTurn)

			endGame()
			return
		end
end -- update()

function love.draw()
	--Reset graphics, set background color, draw wood board
	love.graphics.reset()
	love.graphics.setBackgroundColor( 100, 100, 100 )
	love.graphics.draw(board, cellSize, cellSize, 0, 0.865, 0.865, 0, 0)

	--Draw grid lines
	love.graphics.setColor(0, 0, 0)

	for x = 1, gridSize do
		love.graphics.line(x * cellSize + (cellSize / 2), cellSize + (cellSize / 2), x * cellSize + (cellSize / 2), gridSize * cellSize + (cellSize / 2))
	end
	for y = 1, gridSize do
		love.graphics.line(cellSize + (cellSize / 2), y * cellSize + (cellSize / 2), gridSize * cellSize + (cellSize / 2), y * cellSize + (cellSize / 2))
	end

	--Loop through all tiles
	for x = 1, gridSize do
	  for y = 1, gridSize do
	    if grid[x][y] == "white" then
				--Place white stone image
	    	love.graphics.setColor(255, 255, 255)
				love.graphics.draw(white, x * cellSize + 42, y * cellSize - 2, math.rad(90), 0.45, 0.45, 0, 0)
			elseif grid[x][y] == "black" then
				--Place black stone image
				love.graphics.setColor(0, 0, 0)
				love.graphics.draw(black, x * cellSize + 42, y * cellSize - 2, math.rad(90), 0.45, 0.45, 0, 0)
			end
	  end
	end

	--Draw text for who's turn
	if whiteTurn then
		love.graphics.setColor(255, 255, 255)
		text = "White's Turn"
	else
		love.graphics.setColor(0, 0, 0)
		text = "Black's Turn"
	end

	-- print the text
	love.graphics.setNewFont(20)
	love.graphics.print(text, gridSize * cellSize + 50, 100)
	--

	-- Draw text for player scores
	love.graphics.setColor(255, 255, 100)
	blackScoreText = "Black Score: " .. scores.blackScore
	whiteScoreText = "White Score: " .. scores.whiteScore

	love.graphics.print(blackScoreText, gridSize * cellSize + 50, 150)
	love.graphics.print(whiteScoreText, gridSize * cellSize + 50, 175)
	--

	if blackPassTurn then
		love.graphics.setColor(0, 0, 0)
			love.graphics.print("Black passes turn.", gridSize * cellSize + 50, 215)
	end
	if whitePassTurn then
		love.graphics.setColor(255, 255, 255)
		love.graphics.print("White passes turn.", gridSize * cellSize + 50, 235)
	end

	-- Draw controls
	love.graphics.setColor(160, 160, 160)
	love.graphics.print("r: New Game", gridSize * cellSize + 50, 300)
	love.graphics.print("e: pass turn", gridSize * cellSize + 50, 320)
	love.graphics.print("left click: place stone", gridSize * cellSize + 50, 340)
	--
end -- draw()

--Creates a grid mask of a group of one or more stones
function findGroup(x, y, color, mask)
  if grid[x][y] == color and mask[x][y] == " " then
		mask[x][y] = grid[x][y]

		if x < gridSize then
			findGroup(x+1, y, color, mask)
		end
		if x-1 > 0 then
			findGroup(x-1, y, color, mask)
		end
		if y < gridSize then
			findGroup(x, y+1, color, mask)
		end
		if y-1 > 0 then
			findGroup(x, y-1, color, mask)
		end
  end
end -- findGroup()

--Check if stone placement will cause suicide to group/stone
--returns true if placement is not suicide
function placeCheck(x,y,color)
	grid[x][y] = color
	local mask = {}

	initMask(mask)
	findGroup(x,y,color,mask)

	if checkGroup(color, mask) then
		if color == "white" then
			checkStone(x,y,"black")
		else
			checkStone(x,y,"white")
		end

		if checkGroup(color, mask) then
			grid[x][y] = " "
			return false
		else
			return true
		end
	else
		return true
	end
end -- placeCheck()

--Checks surrounding intersections for groups.
--Then checks if any groups are surrounded
-- (x pos, y pos, <color>)
function checkStone(x,y,color)
	if x+1 < gridSize and grid[x+1][y] == color then
		local mask = {}
		initMask(mask)
		findGroup(x+1,y,color, mask)
		if checkGroup(color, mask) then
			clearStones(mask)
			applyScore()
		end
	end
	if y+1 < gridSize and grid[x][y+1] == color then
		local mask = {}
		initMask(mask)
		findGroup(x,y+1,color, mask)
		if checkGroup(color, mask) then
			clearStones(mask)
			applyScore()
		end
	end
	if x-1 > 0 and grid[x-1][y] == color then
		local mask = {}
		initMask(mask)
		findGroup(x-1,y,color, mask)
		if checkGroup(color, mask) then
			clearStones(mask)
			applyScore()
		end
	end
	if y > 0 and grid[x][y-1] == color then
		local mask = {}
		initMask(mask)
		findGroup(x,y-1,color, mask)
		if checkGroup(color, mask) then
			clearStones(mask)
			applyScore()
		end
	end
end -- checkStone()

--Checks if a group is surrounded
--If any stones in the group have a neighbor that is empty,
--then that group is not surrounded
function checkGroup(color, mask)
	local isSurrounded = true

	for x = 1, gridSize do
	  for y = 1, gridSize do
			if mask[x][y] == color then
				if x+1 < gridSize and grid[x+1][y] == " " then
					isSurrounded = false
				end
				if y+1 < gridSize and grid[x][y+1] == " " then
					isSurrounded = false
				end
				if x-1 > 0 and grid[x-1][y] == " " then
					isSurrounded = false
				end
				if y-1 > 0 and grid[x][y-1] == " " then
					isSurrounded = false
				end
			end
	  end
	end

	return isSurrounded
end -- checkGroup()

-- add capured group size to the player's score
-- TODO uses the global bool whiteTurn to see which score to
-- apply too. I think that it should really use captured
-- group color
function applyScore()
	if whiteTurn then
		scores.whiteScore = scores.whiteScore + groupSize
		totBlack = totBlack - groupSize
	else
		scores.blackScore = scores.blackScore + groupSize
		totWhite = totWhite - groupSize
	end

	groupSize = 0
end -- applyScore

--Initialize a mask
function initMask(mask)
	for x = 1, gridSize do
	  mask[x] = {}
	  for y = 1, gridSize do
		mask[x][y] = " "
	  end
	end
end -- initMask()

-- reset board
-- Sets scores to zero resets all values to their default values
-- called on a keypress event
function resetBoard()
	print("board reset")

	love.load()
end -- resetBoard

-- pass turn. when both players pass turn the game is over
-- will be invoked on a keypress event
function passTurn()
	if whiteTurn then
		whitePassTurn = true
		whiteTurn = false
	else
		blackPassTurn = true
		whiteTurn = true
	end

	print("wt: ", whitePassTurn)
	print("bt: ", blackPassTurn)
end -- passTurn()

-- keypress event handler
-- handles keypresses
function love.keypressed(key)
	if key == "e" then
		passTurn()
	elseif key == "r" then
		resetBoard()
	end
end -- keypress(key)

-- ends the current game
function endGame()
	if whitePassTurn and blackPassTurn then
			print("Game Over")

			if scores.blackScore > scores.whiteScore then
				print("Black wins!")
			elseif scores.whiteScore > scores.blackScore then
				print('White wins!')
			else
				print("Draw")
			end

			if apply then
				-- add remaining stones to score to get territory aprox
				scores.whiteScore = scores.whiteScore + totWhite
				scores.blackScore = scores.blackScore + totBlack

				apply = false
			end

			return -- empty return
	end

	whitePassTurn = false
	blackPassTurn = false
end -- endGame()

--Deletes groups of stones from the board using the group mask
-- if passed nothing then it will clear the board
function clearStones(mask)
	isSurrounded = true
	sound_capture:play()

	if mask then -- if there is a mask clear just the mask
		for x = 1, gridSize do
		  for y = 1, gridSize do
				if mask[x][y] ~= " " then
					grid[x][y] = " "
					groupSize = groupSize + 1 -- size of the groups of stones SHOULD NOT BE GLOBAL BUT THAT'S HOW ITS GOTTA BE!!!!
				end
		  end
		end
	else -- if there are no params then clear the whole board
		for x = 1, gridSize do
			for y = 1, gridSize do
				grid[x][y] = " "
			end
		end
	end
end -- clearStones()
