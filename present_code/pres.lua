--[[
A cool demo of Lua's dankness and confusion
]]

-- ----- BASIC STUFF HERE -----
print("Some people just don't know a good burger")

function isInAndOutTheBestBurger()
  print("Yes")
end

isInAndOutTheBestBurger()

print(8 << 1) -- * 2
print(8 >> 2) -- * 4

local doubleDouble = "#1"

if doubleDouble == "#1" then
  print("That's because five guys is mediocre")
elseif doubleDouble == nil then
  print("Why are you even on earth then")
end

-- ----- OO STUFF HERE -----
-- two ways to add things to an class
-- local yummyBurger = {}
--
-- yummyBurger.typeof = "Yummy"
-- yummyBurger.fake = false
-- yummyBurger.cost = 2.50
--
-- function yummyBurger.toString()
--   print(yummyBurger.typeof)
--   print(yummyBurger.fake)
--   print(yummyBurger.cost)
-- end
--
-- local veggieBurger = {
--   typeof = "monstrosity",
--   fake = true,
--   cost = 10.50,

  -- toString = function (self)
  --   print(self.typeof)
  --   print(self.fake)
  --   print(self.cost)
  -- end,

  -- toString = function ()
  --   print(typeof)
  --   print(fake)
  --   print(cost)
  -- end,
}

-- ----- BASIC FXN ATTRIBUTE CALLS -----
-- yummyBurger.toString()
-- veggieBurger.toString()
-- -----

-- ----- OBJECT INSTANCES -----
-- local doubleDouble = yummyBurger
-- local wopper = yummyBurger
--
-- doubleDouble.toString()
-- wopper.toString()

-- wopper.cost = 5.00

-- doubleDouble.toString()
-- wopper.toString()
--
-- print(doubleDouble)
-- print(wopper)

-- doubleDouble.numPaddies = 2

-- THESE ARE THE SAME TABLE
-- print(doubleDouble.numPaddies)
-- print(wopper.numPaddies)
-- -----

-- ----- SELF REFRENCES -----
-- veggieBurger.toString()
-- veggieBurger.toString(veggieBurger)

-- function veggieBurger:toString()
--   print(self.typeof)
--   print(self.fake)
--   print(self.cost)
-- end
--
-- veggieBurger:toString()

-- ----- METATABLE -----
-- local hawtDog = {}
-- hawtDog.prototype = hawtDog
-- hawtDog.metatable = {__index = hawtDog}
--
-- hawtDog.cond = nil
-- hawtDog.type = nil
--
-- function hawtDog.new()
--   local o = {}
--   setmetatable(o, hawtDog.metatable)
--
--   return o
-- end
--
-- function hawtDog:printf()
--   print(self.cond)
--   print(self.type)
-- end
--
-- function hawtDog:setDog(cond, type)
--   self.cond = cond
--   self.type = type
-- end
--
-- local realDog = hawtDog.new()
-- local fakeDog = hawtDog.new()
--
-- -- print(realDog)
-- -- print(fakeDog)
--
-- realDog:setDog("ranch", "beef")
-- fakeDog:setDog("mustard", "turkey")
--
-- realDog:printf()
-- fakeDog:printf()
